package br.com.lead.Investimento.repositories;

import br.com.lead.Investimento.model.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository  extends CrudRepository<Investimento,Integer> {
}
