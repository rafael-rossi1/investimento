package br.com.lead.Investimento.services;

import br.com.lead.Investimento.dto.CadastroDeInvestimentoDto;
import br.com.lead.Investimento.model.Investimento;
import br.com.lead.Investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoServices {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(CadastroDeInvestimentoDto cadastroDeInvestimentoDto) {
        Investimento investimento = cadastroDeInvestimentoDto.coverterParaInvestimento();
        return investimento;

    }

    public Investimento buscarPeloId(int id){
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);

        if(investimentoOptional.isPresent()){
            Investimento investimento = investimentoOptional.get();
            return investimento;
        }else {
            throw new RuntimeException("Investimento não encontrado");
        }
    }

    public Investimento atualizarInvestimento(int id, Investimento investimento){
        Investimento investimentoDB = buscarPeloId(id);

        investimento.setId(investimentoDB.getId());
        return investimentoRepository.save(investimento);
    }

    public void deletarInvestimento(int id){
        if(investimentoRepository.existsById(id)) {
            investimentoRepository.deleteById(id);
        }else{
            throw new RuntimeException("Investimento não encontrado");
        }
    }
}
