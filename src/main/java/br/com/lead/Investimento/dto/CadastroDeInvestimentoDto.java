package br.com.lead.Investimento.dto;

import br.com.lead.Investimento.model.Investimento;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CadastroDeInvestimentoDto {

    @NotNull(message = "Nome não pode ser nullo")
    @NotBlank(message = "Não pode estar em branco")
    @Size(min = 3, message = "Nome no minimo com 3 caracteres")
    private String nome;
    private double rendimentoAoMes;

    public CadastroDeInvestimentoDto() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public Investimento coverterParaInvestimento(){
        Investimento investimento = new Investimento();
        investimento.setNome(this.nome);
        investimento.setRendimentoAoMes(this.rendimentoAoMes);
        return investimento;
    }

}
